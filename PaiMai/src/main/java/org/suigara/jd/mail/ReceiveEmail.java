package org.suigara.jd.mail;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

public class ReceiveEmail {

	private MimeMessage mimeMessage = null;
	private String saveAttachPath = ""; // 附件下载后的存放目录
	private StringBuffer bodyText = new StringBuffer(); // 存放邮件内容的StringBuffer对象
	private String dateFormat = "yyyy-MM-dd HH:mm:ss"; // 默认的日前显示格式

	public ReceiveEmail() {
	}

	public ReceiveEmail(MimeMessage mimeMessage) {
		this.mimeMessage = mimeMessage;
	}

	public void setMimeMessage(MimeMessage mimeMessage) {
		this.mimeMessage = mimeMessage;
	}

	public String getFrom() throws Exception {
		try {
			Address[] address = mimeMessage.getFrom();
			// String from = "";
			// if (address == null) {
			// from = "";
			// }
			return address[0].toString();
		} catch (Exception e) {
			return "未知";
		}

		// if (personal == null) {
		// personal = "";
		// }
		//
		// String fromAddr = null;
		// if (personal != null || from != null) {
		// fromAddr = personal + "<" + from + ">";
		// System.out.println("发送者是：" + fromAddr);
		// } else {
		// System.out.println("无法获得发送者信息.");
		// }
		// return fromAddr;
	}

	public String getMailAddress(String type) throws Exception {
		String mailAddr = "";
		String addType = type.toUpperCase();

		InternetAddress[] address = null;
		if (addType.equals("TO") || addType.equals("CC")
				|| addType.equals("BCC")) {

			if (addType.equals("TO")) {
				address = (InternetAddress[]) mimeMessage
						.getRecipients(Message.RecipientType.TO);
			} else if (addType.equals("CC")) {
				address = (InternetAddress[]) mimeMessage
						.getRecipients(Message.RecipientType.CC);
			} else {
				address = (InternetAddress[]) mimeMessage
						.getRecipients(Message.RecipientType.BCC);
			}

			if (address != null) {
				for (int i = 0; i < address.length; i++) {
					String emailAddr = address[i].getAddress();
					if (emailAddr == null) {
						emailAddr = "";
					} else {
						emailAddr = MimeUtility.decodeText(emailAddr);
					}
					String personal = address[i].getPersonal();
					if (personal == null) {
						personal = "";
					} else {
						personal = MimeUtility.decodeText(personal);
					}
					String compositeto = personal + "<" + emailAddr + ">";
					mailAddr += "," + compositeto;
				}
				mailAddr = mailAddr.substring(1);
			}
		} else {
			throw new Exception("错误的电子邮件类型!");
		}
		return mailAddr;
	}

	public String getSubject() throws MessagingException {
		String subject = "";
		try {
			subject = MimeUtility.decodeText(mimeMessage.getSubject());
			if (subject == null) {
				subject = "";
			}
		} catch (Exception exce) {
			exce.printStackTrace();
		}
		return subject;
	}

	public String getSentDate() throws Exception {
		Date sentDate = mimeMessage.getSentDate();
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		String strSentDate = format.format(sentDate);
		return strSentDate;
	}

	public String getBodyText() {
		return bodyText.toString();
	}

	public void getMailContent(Part part) throws Exception {

		String contentType = part.getContentType();
		// 获得邮件的MimeType类型

		int nameIndex = contentType.indexOf("name");

		boolean conName = false;

		if (nameIndex != -1) {
			conName = true;
		}

		if (part.isMimeType("text/plain") && conName == false) {
			// text/plain 类型
			bodyText.append((String) part.getContent());
		} else if (part.isMimeType("text/html") && conName == false) {
			// text/html 类型
			bodyText.append((String) part.getContent());
		} else if (part.isMimeType("multipart")) {
			// System.out.println("");
		}
	}

	public boolean getReplySign() throws MessagingException {

		boolean replySign = false;

		String needReply[] = mimeMessage
				.getHeader("Disposition-Notification-To");

		if (needReply != null) {
			replySign = true;
		}
		// if (replySign) {
		// System.out.println("该邮件需要回复");
		// } else {
		// System.out.println("该邮件不需要回复");
		// }
		return replySign;
	}

	public String getMessageId() throws MessagingException {
		String messageID = mimeMessage.getMessageID();
		// System.out.println("邮件ID: " + messageID);
		return messageID;
	}

	public boolean isNew() throws MessagingException {
		boolean isNew = false;
		Flags flags = ((Message) mimeMessage).getFlags();
		Flags.Flag[] flag = flags.getSystemFlags();
		// System.out.println("flags的长度:　" + flag.length);
		for (int i = 0; i < flag.length; i++) {
			if (flag[i] == Flags.Flag.SEEN) {
				isNew = true;
				// System.out.println("seen email...");
				// break;
			}
		}
		return isNew;
	}

	// public boolean isContainAttach(Part part) throws Exception {
	// boolean attachFlag = false;
	// // String contentType = part.getContentType();
	// if (part.isMimeType("multipart")){
	//
	// }
	// }
	// public void saveAttachMent(Part part) throws Exception {
	// String fileName = "";
	// if (part.isMimeType("multipart
	public void setAttachPath(String attachPath) {
		this.saveAttachPath = attachPath;
	}

	public void setDateFormat(String format) throws Exception {
		this.dateFormat = format;
	}

	public String getAttachPath() {
		return saveAttachPath;
	}

	public static void main(String args[]) throws Exception {
		String host = "pop.163.com"; //
		String username = "ww_znufe@163.com"; //
		String password = "ztwvxmrapmypafmy"; //

		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		Store store = session.getStore("pop3");
		store.connect(host, username, password);

		Folder folder = store.getFolder("INBOX");
		folder.open(Folder.READ_ONLY);
		Message message[] = folder.getMessages();
		System.out.println("邮件数量:　" + message.length);

		for (int i = 0; i < message.length; i++) {
			ReceiveEmail re = new ReceiveEmail((MimeMessage) message[i]);
			System.out.println("邮件　" + i + "　主题:　" + re.getSubject());
			System.out.println("邮件　" + i + "　发送时间:　" + re.getSentDate());
			System.out.println("邮件　" + i + "　是否需要回复:　" + re.getReplySign());
			System.out.println("邮件　" + i + "　是否已读:　" + re.isNew());
			// System.out.println("邮件　" + i + "　是否包含附件:　"
			// + re.isContainAttach((Part) message[i]));
			System.out.println("邮件　" + i + "　发送人地址:　" + re.getFrom());
			System.out
					.println("邮件　" + i + "　收信人地址:　" + re.getMailAddress("to"));
			// System.out.println("邮件　" + i + "　抄送:　" +
			// re.getMailAddress("cc"));
			// System.out.println("邮件　" + i + "　暗抄:　" +
			// re.getMailAddress("bcc"));
			re.setDateFormat("yyyy/MM/dd HH:mm:ss");
			System.out.println("邮件　" + i + "　发送时间:　" + re.getSentDate());
			System.out.println("邮件　" + i + "　邮件ID:　" + re.getMessageId());
			// re.getMailContent((Part) message[i]);
			System.out.println("邮件　" + i + "　正文内容:　\r\n" + re.getBodyText());
			// re.setAttachPath("e:\\");
			// re.saveAttachMent((Part) message[i]);
		}
	}
}