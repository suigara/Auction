package org.suigara.jd.bean;

public class GoodsShortcut {
	private long remainTime;
	// 1:竞拍中 2：已结束 0：未开始
	private int auctionStatus;
	private double currentPrice;

	public GoodsShortcut(long remainTime, int auctionStatus, double currentPrice) {
		super();
		this.remainTime = remainTime;
		this.auctionStatus = auctionStatus;
		this.currentPrice = currentPrice;
	}

	public long getRemainTime() {
		return remainTime;
	}

	public int getAuctionStatus() {
		return auctionStatus;
	}

	public double getCurrentPrice() {
		return currentPrice;
	}

}
