package org.suigara.jd.bean;

import org.suigara.jd.constants.JDURLConsts;

public class AuctionInfo {
	private String goodsId;
	private String picUrl;
	private String goodsName;
	private String status;
	private String time;
	private String myPrice;

	public AuctionInfo(String goodsId, String goodsName, String status,
			String time, String myPrice, String picUrl) {
		super();
		this.goodsId = goodsId;
		this.picUrl = picUrl;
		this.goodsName = goodsName;
		this.status = status;
		this.time = time;
		this.myPrice = myPrice;
	}

	public String getGoodsId() {
		return goodsId;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public String getStatus() {
		return status;
	}

	public String getTime() {
		return time;
	}

	public String getMyPrice() {
		return myPrice;
	}

	@Override
	public String toString() {
		return "AuctionInfo [goodsId=" + goodsId + ", picUrl=" + picUrl
				+ ", goodsName=" + goodsName + ", status=" + status + ", time="
				+ time + ", myPrice=" + myPrice + "]";
	}

	public String getUrl() {
		return JDURLConsts.AUCTION_URL + goodsId;
	}

}
