package org.suigara.jd.context;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.suigara.jd.bean.GoodsInfo;

public class GoodsContext {
	private static GoodsContext instance = new GoodsContext();

	private Map<String, Integer> maxPriceConfig = new ConcurrentHashMap<>();
	private Map<String, Integer> addPriceConfig = new ConcurrentHashMap<>();
	private List<GoodsInfo> goodsInfos = new CopyOnWriteArrayList<>();

	private GoodsContext() {

	}

	public static GoodsContext getInstance() {
		return instance;
	}

	public int maxPrice(String goodsid) {
		if (!maxPriceConfig.containsKey(goodsid)) {
			return 10;
		}
		return ((Integer) maxPriceConfig.get(goodsid)).intValue();
	}

	public int addPrice(String goodsid) {
		if (!addPriceConfig.containsKey(goodsid)) {
			return 1;
		}
		return ((Integer) addPriceConfig.get(goodsid)).intValue();
	}

	public void setMaxPrice(String goodsid, Integer maxPrice) {
		maxPriceConfig.put(goodsid, maxPrice);
	}

	public void setAddPrice(String goodsid, Integer addPrice) {
		addPriceConfig.put(goodsid, addPrice);
	}

	public void removeMaxPrice(String goodsid) {
		maxPriceConfig.remove(goodsid);
	}

	public void removeAddPrice(String goodsid) {
		addPriceConfig.remove(goodsid);
	}

	public List<GoodsInfo> getGoodsInfos() {
		return Collections.unmodifiableList(goodsInfos);
	}
	
	public void removeGoodsInfo(String goodsId) {
		// ��Ϊ��д��equals
		this.goodsInfos.remove(new GoodsInfo(goodsId));
	}

	public void addGoodsInfo(GoodsInfo goodsInfo) {
		this.goodsInfos.add(goodsInfo);
	}
	
	public int goodsCount() {
		return goodsInfos.size();
	}
	
}
