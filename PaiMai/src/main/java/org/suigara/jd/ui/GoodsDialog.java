 package org.suigara.jd.ui;
 
 import java.awt.BorderLayout;
 import java.awt.FlowLayout;
 import java.awt.GridLayout;
 import java.awt.event.ActionEvent;
 import java.awt.event.ActionListener;
 import java.text.NumberFormat;
 import javax.swing.JButton;
 import javax.swing.JDialog;
 import javax.swing.JFormattedTextField;
 import javax.swing.JFrame;
 import javax.swing.JLabel;
 import javax.swing.JOptionPane;
 import javax.swing.JPanel;
 import javax.swing.JTextField;
 import org.jsoup.helper.StringUtil;
 
 public class GoodsDialog extends JDialog
   implements ActionListener
 {
   private static final long serialVersionUID = 1L;
   private JTextField goodsNo = new JTextField();
 
   private JFormattedTextField maxPrice = new JFormattedTextField(NumberFormat.getIntegerInstance());
 
   private JFormattedTextField addPrice = new JFormattedTextField(NumberFormat.getIntegerInstance());
 
   private JButton okButtion = new JButton("确定");
   private JButton cancelButtion = new JButton("取消");
 
   private boolean ok = false;
 
   public GoodsDialog(JFrame f)
   {
     super(f);
 
     initUI();
     setSize(400, 200);
     setLocationRelativeTo(null);
   }
 
   private void initUI()
   {
     JPanel mainpanel = new JPanel();
     mainpanel.setLayout(new GridLayout(3, 2));
     mainpanel.add(new JLabel("商品编号"));
     mainpanel.add(this.goodsNo);
     mainpanel.add(new JLabel("最高价格"));
     mainpanel.add(this.maxPrice);
     mainpanel.add(new JLabel("每次加价"));
     mainpanel.add(this.addPrice);
 
     JPanel btnpanel = new JPanel();
     btnpanel.setLayout(new FlowLayout(1));
     btnpanel.add(this.okButtion);
     btnpanel.add(this.cancelButtion);
     this.okButtion.addActionListener(this);
     this.cancelButtion.addActionListener(this);
 
     setModal(true);
 
     setLayout(new BorderLayout());
     add(mainpanel, "Center");
     add(btnpanel, "South");
   }
 
   public void setStateAdd() {
     this.goodsNo.setEditable(true);
     this.goodsNo.setText("");
     this.maxPrice.setValue(Integer.valueOf(10));
     this.addPrice.setValue(Integer.valueOf(1));
   }
 
   public void setStateEdit() {
     this.goodsNo.setEditable(false);
   }
 
   public void setGoodsNo(String no) {
     this.goodsNo.setText(no);
   }
 
   public void setMaxPrice(Integer maxPrice) {
     this.maxPrice.setValue(maxPrice);
   }
 
   public void setAddPrice(Integer addPrice) {
     this.addPrice.setValue(addPrice);
   }
 
   public String getGoodsNo() {
     return this.goodsNo.getText().trim();
   }
 
   public Integer getMaxPrice() {
     if (this.maxPrice.getValue() == null) {
       return null;
     }
     return Integer.valueOf(((Number)this.maxPrice.getValue()).intValue());
   }
 
   public Integer getAddPrice() {
     if (this.addPrice.getValue() == null) {
       return null;
     }
     return Integer.valueOf(((Number)this.addPrice.getValue()).intValue());
   }
 
   public boolean isOk()
   {
     return this.ok;
   }
 
   public void actionPerformed(ActionEvent e)
   {
     if (e.getSource() == this.okButtion) {
       if (StringUtil.isBlank(getGoodsNo())) {
         JOptionPane.showMessageDialog(this, "请输入商品编号");
         return;
       }
       if ((getMaxPrice() == null) || (getMaxPrice().intValue() < 2)) {
         JOptionPane.showMessageDialog(this, "请输入合法的最大金额");
         return;
       }
       if ((getAddPrice() == null) || (getAddPrice().intValue() < 1)) {
         JOptionPane.showMessageDialog(this, "请输入每次加价");
         return;
       }
       this.ok = true;
     } else {
       this.ok = false;
     }
     setVisible(false);
   }
 }
