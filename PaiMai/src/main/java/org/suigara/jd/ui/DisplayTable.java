package org.suigara.jd.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.suigara.jd.bean.GoodsInfo;
import org.suigara.jd.constants.AuctionConst;
import org.suigara.jd.constants.JDURLConsts;
import org.suigara.jd.context.GoodsContext;
import org.suigara.jd.request.UnSessionUtils;
import org.suigara.jd.utils.StringUtils;

@SuppressWarnings("serial")
public class DisplayTable extends JTable {
	private static final int ID_INX = 0;
	private static final int NAME_INX = 1;
	private static final int CURPRICE_INX = 2;
	private static final int JDPRICE_INX = 3;
	private static final int MYPRICE_INX = 4;
	private static final int PAIMAISTATUS_INX = 5;
	private static final int GOODSSTATUS_INX = 6;
	private static final int PLACE_INX = 7;
	private static final int STARTTIME_INX = 8;
	private static final int ENDTIME_INX = 9;
	private static final int ELSETIME_INX = 10;
	private static final int VMEMO_INX = 11;
	private static final int SETTING_INX = 12;
	private DefaultTableModel model;
	private Map<String, Integer> mapRowNo = new ConcurrentHashMap<>();

	public DisplayTable() {
		initUI();
	}

	public Object getValueAt(int row, int column) {
		return model.getValueAt(row, column);
	}

	public void setValueAt(Object aValue, int row, int column) {
		model.setValueAt(aValue, row, column);
	}

	public String getGoodsId(int rowNo) {
		return Objects.toString(getValueAt(rowNo, ID_INX));
	}

	public String getPaimaiStauts(int rowNo) {
		return Objects.toString(getValueAt(rowNo, PAIMAISTATUS_INX));
	}

	public void updateGoodsName(String goodsId, String goodsName) {
		updateById(goodsId, NAME_INX, goodsName);
	}

	public void updateJDPrice(String goodsId, String jdPrice) {
		updateById(goodsId, JDPRICE_INX, jdPrice);
	}

	public void updateCurrentPrice(String goodsId, Double currentPrice) {
		updateById(goodsId, CURPRICE_INX, currentPrice);
	}

	public void updateMyPrice(String goodsId, Integer myPrice) {
		updateById(goodsId, MYPRICE_INX, myPrice);
	}

	public void updateGoodsPlace(String goodsId, String goodsPlace) {
		updateById(goodsId, PLACE_INX, goodsPlace);
	}

	public void updateGoodsStatus(String goodsId, String status) {
		updateById(goodsId, GOODSSTATUS_INX, status);
	}

	public void updatePaimaiStatus(String goodsId, String status) {
		updateById(goodsId, PAIMAISTATUS_INX, status);
	}

	public void updateMemo(String goodsId, String momo) {
		updateById(goodsId, VMEMO_INX, momo);
	}

	public void updateRemainTime(String goodsId, long elseTime) {
		updateById(goodsId, ELSETIME_INX, StringUtils.formatTime(elseTime));
	}

	public void updateStartTime(String goodsId, long startTimeMili) {
		updateById(goodsId, STARTTIME_INX,
				StringUtils.formatDateTime(startTimeMili));
	}

	public void updateEndTime(String goodsId, long endTimeMili) {
		updateById(goodsId, ENDTIME_INX,
				StringUtils.formatDateTime(endTimeMili));
	}

	private void updateById(String goodsId, int col, Object value) {
		if (!existGoodsId(goodsId)) {
			return;
		}
		int rowno = ((Integer) this.mapRowNo.get(goodsId)).intValue();
		setValueAt(value, rowno, col);
	}

	private void initUI() {
		this.model = new DefaultTableModel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		this.model.setColumnIdentifiers(new String[] { "编号", "名称", "当前价格",
				"京东价", "我的价格", "拍卖状态", "商品状态", "所在地", "开始时间", "结束时间", "剩余时间",
				"备注", "最高价/加价" });
		setDefaultRenderer(Object.class, createTableCellRenderer());
		getSelectionModel().setSelectionMode(
				ListSelectionModel.SINGLE_SELECTION);
		setModel(this.model);

		setRowHeight(25);
		getColumnModel().getColumn(STARTTIME_INX).setPreferredWidth(120);
		getColumnModel().getColumn(ENDTIME_INX).setPreferredWidth(120);
		getColumnModel().getColumn(VMEMO_INX).setPreferredWidth(150);
		getColumnModel().getColumn(ELSETIME_INX).setPreferredWidth(100);
		getColumnModel().getColumn(NAME_INX).setPreferredWidth(120);
		getColumnModel().getColumn(MYPRICE_INX).setPreferredWidth(60);
		getColumnModel().getColumn(JDPRICE_INX).setPreferredWidth(60);
		getColumnModel().getColumn(CURPRICE_INX).setPreferredWidth(60);
		getColumnModel().getColumn(PLACE_INX).setPreferredWidth(60);
		getColumnModel().getColumn(0).setPreferredWidth(60);

		addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() != 1) {
					return;
				}
				if (e.getClickCount() == 2) {
					int selectRow = getSelectedRow();
					if (selectRow < 0) {
						return;
					}
					String goodsNo = Objects.toString(model.getValueAt(
							selectRow, 0));
					try {
						Desktop.getDesktop().browse(
								new URI(JDURLConsts.AUCTION_URL
										+ goodsNo));
					} catch (IOException iee) {
						iee.printStackTrace();
					} catch (URISyntaxException localURISyntaxException) {
					}
				}
			}
		});

	}

	private DefaultTableCellRenderer createTableCellRenderer() {
		return new DefaultTableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table,
					Object value, boolean isSelected, boolean hasFocus,
					int row, int column) {
				Component comp = super.getTableCellRendererComponent(table,
						value, isSelected, hasFocus, row, column);

				Object status = getPaimaiStauts(row);

				if (AuctionConst.STATUS_END.equals(status)) {
					comp.setBackground(Color.GRAY);
				} else if (AuctionConst.STATUS_NOSTART.equals(status)) {
					comp.setBackground(Color.ORANGE);
				} else if (AuctionConst.STATUS_RUNNING.equals(status)) {
					comp.setBackground(Color.GREEN);
				} else if (AuctionConst.STATUS_GIVEUP.equals(status)) {
					comp.setBackground(Color.RED);
				}
				if (isSelected) {
					comp.setBackground(comp.getBackground().darker());
				}

				comp.setFont(new Font(comp.getFont().getFontName(), Font.BOLD,
						comp.getFont().getSize()));

				return comp;
			}
		};
	}

	private void addRow(String[] strings) {
		model.addRow(strings);
	}

	public void addGoods(String goodsId, Integer maxPrice, Integer addPrice) {
		if (mapRowNo.containsKey(goodsId)) {
			throw new IllegalArgumentException("该商品已经存在，请不要重复添加");
		}
		addGoodsDirect(goodsId, maxPrice, addPrice);
	}

	private Lock lock = new ReentrantLock();

	public void addGoodsDirect(String goodsId, Integer maxPrice,
			Integer addPrice) {
		if (mapRowNo.containsKey(goodsId)) {
			return;
		}
		try {
			lock.lock();

			GoodsInfo goodsInfo = UnSessionUtils.getGoods(goodsId);
			GoodsContext.getInstance().addGoodsInfo(goodsInfo);
			int rowNO = Integer
					.valueOf(GoodsContext.getInstance().goodsCount() - 1);
			this.mapRowNo.put(goodsId, rowNO);

			addRow(new String[] { goodsId });
			int rowno = updateGoodsInfo(goodsInfo);
			editGoods(rowno, goodsId, maxPrice, addPrice);
			getSelectionModel().setSelectionInterval(rowNO, rowNO);
			repaint();
		} catch (Throwable e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage());
		} finally {
			lock.unlock();
		}
	}

	public void removeGoods(String goodsId) {
		try {
			lock.lock();

			if (!existGoodsId(goodsId)) {
				return;
			}

			int rowNo = getRowNoByGoodsID(goodsId);
			model.removeRow(rowNo);
			mapRowNo.remove(goodsId);
			
			GoodsContext.getInstance().removeGoodsInfo(goodsId);

			int rowCnt = model.getRowCount();
			for (int i = rowNo; i < rowCnt; i++) {
				String currentId = (String) getValueAt(i, ID_INX);
				mapRowNo.put(currentId, i);
			}

			repaint();
		} catch (Throwable e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage());
		} finally {
			lock.unlock();
		}
	}

	public List<GoodsInfo> getGoodsInfos() {
		return GoodsContext.getInstance().getGoodsInfos();
	}

	public void editGoods(int selectRow, String goodsNo, Integer maxPrice,
			Integer addPrice) {
		setValueAt(maxPrice + "/" + addPrice, selectRow, SETTING_INX);
		GoodsContext.getInstance().setMaxPrice(goodsNo, maxPrice);
		GoodsContext.getInstance().setAddPrice(goodsNo, addPrice);
	}

	public int updateGoodsInfo(GoodsInfo goodsInfo) {
		int rowno = ((Integer) this.mapRowNo.get(goodsInfo.getGoodsId()))
				.intValue();
		updateGoodsName(goodsInfo.getGoodsId(), goodsInfo.getGoodsName());
		updateJDPrice(goodsInfo.getGoodsId(), goodsInfo.getJDPriceString());
		updateGoodsPlace(goodsInfo.getGoodsId(), goodsInfo.getPlace());
		updateGoodsStatus(goodsInfo.getGoodsId(), goodsInfo.getStatus());
		updateStartTime(goodsInfo.getGoodsId(), goodsInfo.getStartTimeMili());
		updateEndTime(goodsInfo.getGoodsId(), goodsInfo.getEndTimeMili());
		return rowno;
	}

	public int getRowNoByGoodsID(String goodsid) {
		return ((Integer) this.mapRowNo.get(goodsid)).intValue();
	}

	public boolean existGoods(GoodsInfo goodsInfo) {
		return existGoodsId(goodsInfo.getGoodsId());
	}

	public boolean existGoodsId(String goodsId) {
		return mapRowNo.containsKey(goodsId);
	}

}
