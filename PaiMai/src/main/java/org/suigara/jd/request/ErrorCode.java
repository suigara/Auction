package org.suigara.jd.request;

import java.util.HashMap;
import java.util.Map;

public class ErrorCode {
	private static Map<String, String> codeMessage = new HashMap<>();
	static {
		codeMessage.put("453", "请您不要连续出价~");
		codeMessage.put("451", "有人比你出的价格高了哦，再加点价吧！");
		codeMessage.put("452", "拍卖尚未开始，暂不能出价~");
		codeMessage.put("450", "拍卖已经结束，您略晚了一步~");
		codeMessage.put("457", "您暂无参拍资格~");
		codeMessage.put("467", "银牌及以上会员京豆小于等于0，不能出价!");
		codeMessage.put("459", "出价不能低于商品起拍价~");
		codeMessage.put("460", "每次加价不得低于最低加价~");
		codeMessage.put("461", "每次加价不得高于最高加价~");
		codeMessage.put("462", "您所出的价格不能超过该商品的京东价~");
		codeMessage.put("463", "出价格式不对！所出价格必须为正整数~");
		codeMessage.put("464", "您来晚了一步，本次拍卖已关闭~");
		codeMessage.put("465", "您来晚了一步，本次拍卖已删除~");
		codeMessage.put("466", "您的账户异常，请稍后再试~");
		codeMessage.put("468", "出价异常，请稍后再试~");
		codeMessage.put("469", "尊敬的京东会员,您的京豆需要大于0才可参与拍卖！");
		codeMessage.put("470", "尊敬的京东会员,您的京豆需要大于等于0才可参与拍卖！");
		codeMessage
				.put("471",
						"尊敬的京东会员,为了保障您的账户安全,请先设置京东支付密码!,<a href='http://safe.jd.com/user/paymentpassword/safetyCenter.action' target='blank'>【点我设置支付密码】</a>");
		codeMessage.put("4201", "您同时在拍的商品不得超过5个！");
		codeMessage.put("4202", "您在夺宝岛获拍且未支付的拍卖不得超过5个!");
		codeMessage.put("4203", "您于夺宝岛在拍或未支付的商品总数不得超过5个!");

		codeMessage.put("400", "系统异常，请稍后再试~");
		codeMessage.put("402", "系统响应异常，请稍后再试~");
	}

	public static String getMessage(String code) {
		return codeMessage.get(code);
	}
}
