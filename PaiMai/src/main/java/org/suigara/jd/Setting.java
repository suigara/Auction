package org.suigara.jd;

import java.io.IOException;
import java.util.Properties;

import org.suigara.jd.log.Logger;

public class Setting {
	
	private static long rateTime = 1000L;
	private static long doPriceTime = 5000L;

	private static String userName;
	private static String password;

	static {
		Properties prop = new Properties();
		try {
			prop.load(Setting.class.getResourceAsStream("/setting.properties"));
			rateTime = Integer.valueOf(prop.getProperty("rateTime")).intValue();
			doPriceTime = Integer.valueOf(prop.getProperty("doPriceTime"))
					.intValue();
			Logger.info("rateTime(扫描时间)=" + rateTime + " ms");
			Logger.info("doPriceTime(剩余多久出价)=" + doPriceTime + " ms");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public synchronized static long getRateTime() {
		return rateTime;
	}

	public static long doPriceTime() {
		return doPriceTime;
	}

	public static void setRateTime(long newTime) {
		rateTime = newTime;
	}

	public static void setDoPriceTime(long newTime) {
		doPriceTime = newTime;
	}

	public static void setUserName(String userName) {
		Setting.userName = userName;
		Logger.info("用户名=" + userName);
	}

	public static void setPassword(String password) {
		Setting.password = password;
	}

	/**
	 * @return the userName
	 */
	public static String getUserName() {
		return userName;
	}

	/**
	 * @return the password
	 */
	public static String getPassword() {
		return password;
	}
}
