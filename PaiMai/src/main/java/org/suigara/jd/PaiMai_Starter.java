package org.suigara.jd;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.suigara.jd.ui.Login;

public class PaiMai_Starter {
	public static void main(String[] args) {
		try {
			for (UIManager.LookAndFeelInfo info : UIManager
					.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception localException) {
		}
		final AuctionSwingUI auction = new AuctionSwingUI();

		while(!login(auction)){;}
		
		auction.loadGoods(AuctionSwingUI.goodsFile);
		auction.startRefreshGoods();
		auction.startMonitor();
		auction.startAutoSearch();

	}

	private static boolean login(final AuctionSwingUI auction) {
		if (0 != Login.showLoginGUI(auction)) {
			auction.shutdown();
			return true;
		}
		String userName = Login.usernameField.getText();
		@SuppressWarnings("deprecation")
		String password = Login.passwordField.getText();
		Setting.setUserName(userName);
		Setting.setPassword(password);
		boolean login = auction.login();
		if (!login) {
			JOptionPane.showMessageDialog(auction, "��¼ʧ��!");
			Login.saveLoginInfo(userName, password, false);
			
		}
		return login;
	}
}
