package org.suigara.jd.utils;

import org.suigara.jd.constants.JDURLConsts;

import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.cp.api.WxCpInMemoryConfigStorage;
import me.chanjar.weixin.cp.api.WxCpServiceImpl;
import me.chanjar.weixin.cp.bean.WxCpMessage;
import me.chanjar.weixin.cp.bean.WxCpMessage.WxArticle;

public class WeixinUtils {
	static final String appid = "1";
	public static void main(String[] args) throws WxErrorException {
		WxCpInMemoryConfigStorage config = createServiceConfig(appid);

		WxCpServiceImpl wxCpService = new WxCpServiceImpl();
		wxCpService.setWxCpConfigStorage(config);

		/**
		 * UserID列表（消息接收者，多个接收者用‘|’分隔）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
		 */
		String userId = "@all";
		WxCpMessage message = createNews(
				userId,
				appid,
				"（test）贝亲（Pigeon）哺喂勺组 DA39",
				"（test）成交时间:2015-06-14 18:00:00,出价:8.0",
				"http://img11.360buyimg.com/n1/jfs/t1039/4/855397286/67354/bbe028/55533041N1a376b27.jpg",
				JDURLConsts.AUCTION_URL+"/10577907");
		wxCpService.messageSend(message);

	}

	private static WxCpInMemoryConfigStorage createServiceConfig(String appid) {
		WxCpInMemoryConfigStorage config = new WxCpInMemoryConfigStorage();
		config.setCorpId("wxa2d901531e428eda"); // 设置微信企业号的appid
		config.setCorpSecret("S3LGnqx8AbUO3JP1jHz9TBt_0Y1Fbxt82nobJGOeyyUDdOLdWcgSzAtzFqe-5ymO"); // 设置微信企业号的app
																									// corpSecret
		config.setAgentId(appid); // 设置微信企业号应用ID
//		config.setToken("3a2FCcGjszijfT"); // 设置微信企业号应用的token
//		config.setAesKey("MCx4YMqX0VK1NdOsRsUi4QQtCvlzqn90MxP6ygLL8wj"); // 设置微信企业号应用的EncodingAESKey
		return config;
	}

	

	public static boolean sendWeiXin(String title, String description,
			String picUrl, String url) throws WxErrorException {

		WxCpInMemoryConfigStorage config = createServiceConfig(appid);
		WxCpServiceImpl wxCpService = new WxCpServiceImpl();
		wxCpService.setWxCpConfigStorage(config);

		/**
		 * UserID列表（消息接收者，多个接收者用‘|’分隔）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
		 */
		String userId = "wangwei|weiqs";
		WxCpMessage message2 = createNews(userId, appid, title, description,
				picUrl, url);
		wxCpService.messageSend(message2);
		return true;
	}

	private static WxCpMessage createNews(String userId, String appid,
			String title, String description, String picUrl, String url) {
		WxArticle wxArticle = new WxArticle();
		wxArticle.setUrl(url);
		wxArticle.setTitle(title);
		wxArticle.setPicUrl(picUrl);
		wxArticle.setDescription(description);
		WxCpMessage message2 = WxCpMessage.NEWS().agentId(appid).toUser(userId)
				.addArticle(wxArticle).build();
		return message2;
	}

	// private static WxCpMessage createTextMessage(String userId, String appid)
	// {
	// // @all 全部
	// WxCpMessage message1 = WxCpMessage.TEXT().agentId(appid).toUser(userId)
	// .content("报警测试").build();
	// return message1;
	// }

}
