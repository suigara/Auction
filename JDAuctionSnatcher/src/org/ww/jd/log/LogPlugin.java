package org.ww.jd.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogPlugin {
	private static final Logger logger = LoggerFactory.getLogger(LogPlugin.class);
	
	static Logger getLogger(){
		return logger;
	}

}
