package org.ww.jd.auction;

public class GoodsInfo {
	private String className;
	private String goodsId;
	private String goodsName;
	private String attachlist;
	private Double JDPrice;
	private Long startTimeMili;
	private Long endTimeMili;
	private Double currentPrice;
	private String place;
	private String status;

	public Double getJDPriceDouble() {
		return this.JDPrice;
	}

	public String getJDPriceString() {
		if (this.JDPrice == -1) {
			return "暂无报价";
		}
		return String.valueOf(JDPrice);
	}

	public void setJDPrice(Double jDPrice) {
		this.JDPrice = jDPrice;
	}

	public GoodsInfo(String goodsId) {
		this.goodsId = goodsId;
	}

	public String getAttachlist() {
		return attachlist;
	}

	public void setAttachlist(String attachlist) {
		this.attachlist = attachlist;
	}

	public String getGoodsId() {
		return this.goodsId;
	}

	public String getGoodsName() {
		return this.goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public Long getStartTimeMili() {
		return this.startTimeMili;
	}

	public void setStartTimeMili(Long startTimeMili) {
		this.startTimeMili = startTimeMili;
	}

	public Long getEndTimeMili() {
		return this.endTimeMili;
	}

	public void setEndTimeMili(Long endTimeMili) {
		this.endTimeMili = endTimeMili;
	}

	public Double getCurrentPrice() {
		return this.currentPrice;
	}

	public void setCurrentPrice(Double currentPrice) {
		this.currentPrice = currentPrice;
	}

	public String getPlace() {
		return this.place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public void update(GoodsInfo newGoodsInfo) {
		this.startTimeMili = newGoodsInfo.startTimeMili;
		this.endTimeMili = newGoodsInfo.endTimeMili;
		this.goodsName = newGoodsInfo.goodsName;
	}

	public String toString() {
		return "编号=" + this.goodsId + ";名称=" + this.goodsName + ";当前价格="
				+ this.currentPrice + ";京东价=" + this.JDPrice + ";所在地="
				+ this.place + ";" + this.status;
	}
}