package org.ww.jd.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.ww.jd.auction.IgnorException;

public class HttpClientUtils {
	public static Document doGetHtml( String url) {
		String content = doGetStringWithReferer( url, null);
		return Jsoup.parse(content);
	}

	public static JSONObject doGetJson(
			String url, String curUrl) {
		String doGetString = doGetStringWithReferer( url, curUrl);

		try {

			JSONObject jsobj = new JSONObject(doGetString);
			return jsobj;
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	public static JSONArray doGetJsonArray(
			String url) {
		String doGetString = doGetString( url);
		try {

			JSONArray jsobj = new JSONArray(doGetString);
			return jsobj;
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}

	}

	public static String doGetString( String url) {
		return doGetStringWithReferer( url, null);
	}

	private static String doGetStringWithReferer(
			String url, String curUrl) {
		HttpGet httpGet = new HttpGet(url);
		// JDURLConsts.AUCTION_URL+"/"+goods.getGoodsId()
		if (curUrl != null) {
			httpGet.setHeader("Referer", curUrl);
		}
		CloseableHttpResponse response = null;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {
			response = httpclient.execute(httpGet);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("errorcode="
						+ response.getStatusLine().getStatusCode());
			}
			HttpEntity httpEntity = response.getEntity();

			if (httpEntity == null) {
				throw new RuntimeException("httpEntity=null");
			}
			InputStream content = httpEntity.getContent();
			String str = getContentString(content);
			content.close();
			
			return str;
		} catch (IOException e) {
			throw new IgnorException(e);
		} finally {
			if (httpGet != null) {
				httpGet.abort();
			}
			if (response != null) {
				try {
					response.close();
				} catch (IOException localIOException2) {
				}
			}
			if(httpclient !=null){
				try {
					httpclient.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public static String getContentString(InputStream content) {
		StringBuilder entityStringBuilder = new StringBuilder();
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new InputStreamReader(content,
					"UTF-8"), 8192);
			String line = null;

			while ((line = bufferedReader.readLine()) != null) {
				entityStringBuilder.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
			try {
				if (bufferedReader != null)
					bufferedReader.close();
			} catch (IOException localIOException1) {
			}
		} finally {
			try {
				if (bufferedReader != null)
					bufferedReader.close();
			} catch (IOException localIOException2) {
			}
		}
		return entityStringBuilder.toString();
	}
}
