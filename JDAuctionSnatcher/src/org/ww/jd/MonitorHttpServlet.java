package org.ww.jd;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.ww.jd.log.Logger;
import org.ww.jd.snatch.Snatcher;

public class MonitorHttpServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8148915328636994708L;

	@Override
	public void init() throws ServletException {
		super.init();
		final Snatcher s = new Snatcher();
		ScheduledExecutorService sesPrepare = Executors
				.newScheduledThreadPool(1);
		sesPrepare.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				Logger.info("prepareIds start");
				try {
					int newAddIds = s.prepareIds();
					Logger.info("prepareIds end, newAddIds number = ["
							+ newAddIds + "]");
				} catch (Throwable e) {
					Logger.error(e);
					Logger.info("prepareIds end, error[" + e.getMessage() + "]");
				}
			}
		}, 0, 30, TimeUnit.MINUTES);

		ScheduledExecutorService sesExcute = Executors
				.newScheduledThreadPool(1);
		sesExcute.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				Logger.info("excuteFromWaitings start");
				int excuteNumber = s.excuteFromWaitings();
				Logger.info("excuteFromWaitings end,excute number=["
						+ excuteNumber + "]");

			}
		}, 0, 10, TimeUnit.MINUTES);
	}

}
