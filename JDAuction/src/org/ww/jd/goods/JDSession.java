package org.ww.jd.goods;

import static org.ww.jd.utils.HttpClientUtils.doGetString;
import static org.ww.jd.utils.HttpClientUtils.getContentString;

import java.awt.Desktop;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.ww.jd.JDURLConsts;
import org.ww.jd.Setting;
import org.ww.jd.auction.AuctionInfo;
import org.ww.jd.auction.GoodsInfo;
import org.ww.jd.auction.IgnorException;
import org.ww.jd.auction.StringUtils;
import org.ww.jd.auction.UnSessionUtils;
import org.ww.jd.log.Logger;
import org.ww.jd.utils.HttpClientUtils;

public class JDSession {
	// The configuration items
	private String userName;
	private String password;

	private static String redirectURL = "http://order.jd.com/center/list.action";
	private static String loginUrl = "http://passport.jd.com/uc/login";
	// Don't change the following URL
	private static String renRenLoginURL = "https://passport.jd.com/uc/loginService";

	// The HttpClient is used in one session
	private CloseableHttpClient httpclient = HttpClients.createDefault();;

	public JDSession() {
		super();

	}

	public static void main(String[] args) throws IOException,
			URISyntaxException {
		String url = ("http://auction.jd.com/json/paimai/bid?t="
				+ Calendar.getInstance().getTime().getTime() + "&dealId=9716009&price=3");
		Desktop.getDesktop().browse(new URI(url));
		Date d = new Date(1423832399936L);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Logger.info(sdf.format(d));
	}

	private boolean login = false;

	public boolean login(AuthCodeListener l) {
		try {
			userName = Setting.getUserName();
			password = Setting.getPassword();
			login = loginImpl(l);

		} catch (Exception e) {
			login = false;
		}
		return login;
	}

	@SuppressWarnings("deprecation")
	private boolean loginImpl(AuthCodeListener l) {
		Map<String, String> map = getParams(l);

		HttpPost httpost = new HttpPost(renRenLoginURL + "?nr=1&uuid="
				+ map.get("uuid") + "&r=" + Math.random());
		List<BasicNameValuePair> nvps = new ArrayList<BasicNameValuePair>();
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			String value = map.get(key);
			nvps.add(new BasicNameValuePair(key, value));
		}

		try {
			httpost.setEntity(new UrlEncodedFormEntity(
					(List<? extends NameValuePair>) nvps, HTTP.UTF_8));
			HttpResponse response = httpclient.execute(httpost);
			String result = getContentString(response.getEntity().getContent());
			boolean loginSuccess = result.contains("success")
					&& result.contains(redirectURL);
			if (!loginSuccess) {
				Logger.info("登录失败:" + result);
			}
			return loginSuccess;
		} catch (Exception e) {
			Logger.error(e);
			return false;
		} finally {
			httpost.abort();
		}
	}

	public boolean doPrice(GoodsInfo goods, int newPrice) {
		if (login) {
			long time = Calendar.getInstance().getTime().getTime();

			try {
				JSONObject json = doGetJson2(httpclient,
						JDURLConsts.AUCTION_URL+"/"+goods.getGoodsId(),
						JDURLConsts.AUCTION_URL + "json/paimai/bid?t=" + time
								+ "&dealId=" + goods.getGoodsId() + "&price="
								+ newPrice);

				String message = json.getString("message");
				boolean success = message != null
						&& json.getString("message").contains("success!");
				if (!success) {
					String code = StringUtils.toString(json.get("code"));
					if (code == null || code.isEmpty()) {
						Logger.info("[" + goods.getGoodsId()
								+ "]doPrice error:" + json.toString());
					} else {
						String errorMsg = ErrorCode.getMessage(code);
						Logger.info("[" + goods.getGoodsId() + "]"
								+ code + ":" + errorMsg);
					}
				}
				return success;
			} catch (Exception e) {
				Logger.error(e);
				UnSessionUtils.doPriceWithBrowser(goods, newPrice);
				return true;
			}
		} else {
			UnSessionUtils.doPriceWithBrowser(goods, newPrice);
			return true;
		}
	}

	private static JSONObject doGetJson2(CloseableHttpClient httpclient,
			String oldUrl, String url) {
		HttpGet httpGet = new HttpGet(url);
		httpGet.setHeader("Referer", oldUrl);// "http://auction.jd.com/9714896"
		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(httpGet);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("errorcode="
						+ response.getStatusLine().getStatusCode());
			}
			HttpEntity httpEntity = response.getEntity();

			if (httpEntity == null) {
				throw new RuntimeException("httpEntity=null");
			}
			InputStream content = httpEntity.getContent();
			String str = getContentString(content);
			content.close();

			JSONObject jsobj = new JSONObject(str);
			return jsobj;
		} catch (IOException e) {
			throw new IgnorException(e);
		} catch (JSONException e) {
			Logger.error(e);
			throw new RuntimeException(e);
		} finally {
			if (response != null)
				try {
					response.close();
				} catch (IOException localIOException2) {
				}

		}
	}

	public List<AuctionInfo> getAuctionResult() {
		Document doc = HttpClientUtils.doGetHtml(httpclient,
				"http://auction.jd.com/myauction.action");
		Elements elements = doc.getElementsByClass("tb-void");
		Element table = elements.get(0);
		Elements rows = table.getElementsByTag("tr");
		int rowCnt = rows.size();
		List<AuctionInfo> myGoods = new ArrayList<AuctionInfo>();
		for (int i = 0; i < rowCnt; i++) {
			Element rowData = rows.get(i);
			Elements headers = rowData.getElementsByTag("th");
			if (headers.size() > 0) {
				continue;
			}
			// <th width="100">商品名称</th>
			//
			// <th width="120">夺宝编号</th>
			// <th width="130">交易类型</th>
			// <th width="130">交易价格</th>
			// <th width="130">交易时间</th>
			// <th width="92">状态</th>
			// <th width="140">操作</th>

			Elements cells = rowData.getElementsByTag("td");
			Element idElement = cells.get(1);
			String goodsId = idElement.child(0).text();
			Element priceElement = cells.get(3);
			String priceStr = priceElement.text();

			Element timeElement = cells.get(4);
			String timeStr = timeElement.child(0).text().trim();

			Element operatorElement = cells.get(6);
			String operatorStr = operatorElement.child(0).text().trim();
			myGoods.add(new AuctionInfo(goodsId, operatorStr, timeStr, priceStr
					.replace("￥", "")));
		}

		return myGoods;
	}

	private Map<String, String> getParams(AuthCodeListener l) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("ReturnUrl", redirectURL);
		map.put("loginname", userName);
		map.put("nloginpwd", password);
		map.put("loginpwd", password);

		// uuid and cookie
		String str = doGetString(httpclient, loginUrl);
		Document doc = Jsoup.parse(str);
		Element authcode = doc.getElementById("o-authcode");
		Element authcodeImg = authcode.getElementsByTag("img").get(0);
		String authCodeUrl = authcodeImg.attr("src");
		if (authCodeUrl == null || authCodeUrl.isEmpty()) {
			authCodeUrl = authcodeImg.attr("src2");
		}
		String authCodeStr = l.authCode(authCodeUrl);
		map.put("authcode", authCodeStr.toString());
		String strs1[] = str.split("name=\"uuid\" value=\"");
		String strs2[] = strs1[1].split("\"/>");
		String uuid = strs2[0];
		map.put("uuid", uuid);
		String str3s[] = strs1[1]
				.split("<span class=\"clr\"></span><input type=\"hidden\" name=\"");
		String strs4[] = str3s[1].split("/>");
		String strs5[] = strs4[0].trim().split("\"");
		String key = strs5[0];
		String value = strs5[2];
		map.put(key, value);
		return map;
	}

	public void closeSession() {
		try {
			httpclient.close();
		} catch (IOException e) {
			Logger.error(e);
		}
	}

}