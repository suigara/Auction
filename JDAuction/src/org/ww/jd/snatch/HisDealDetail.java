package org.ww.jd.snatch;

import java.sql.Timestamp;

public class HisDealDetail {
	private String goodsid;
	private String goodsname;
	private Double jdprice;
	private String status;
	private String place;
	private Double dealprice;
	private String dealuser;
	private Timestamp starttime;
	private Timestamp dealdatetime;
	private Timestamp ts;

	public String getGoodsid() {
		return goodsid;
	}

	public void setGoodsid(String goodsid) {
		this.goodsid = goodsid;
	}

	public String getGoodsname() {
		return goodsname;
	}

	public void setGoodsname(String goodsname) {
		this.goodsname = goodsname;
	}

	public Double getDealprice() {
		return dealprice;
	}

	public void setDealprice(Double dealprice) {
		this.dealprice = dealprice;
	}

	public String getDealuser() {
		return dealuser;
	}

	public void setDealuser(String dealuser) {
		this.dealuser = dealuser;
	}

	public Timestamp getDealdatetime() {
		return dealdatetime;
	}

	public void setDealdatetime(Timestamp dealdatetime) {
		this.dealdatetime = dealdatetime;
	}

	public Timestamp getTs() {
		return ts;
	}

	public void setTs(Timestamp ts) {
		this.ts = ts;
	}

	public Double getJdprice() {
		return jdprice;
	}

	public void setJdprice(Double jdprice) {
		this.jdprice = jdprice;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Timestamp getStarttime() {
		return starttime;
	}

	public void setStarttime(Timestamp starttime) {
		this.starttime = starttime;
	}

	@Override
	public String toString() {
		return "HisDealDetail [goodsid=" + goodsid + ", goodsname=" + goodsname
				+ ", jdprice=" + jdprice + ", status=" + status + ", place="
				+ place + ", dealprice=" + dealprice + ", dealuser=" + dealuser
				+ ", starttime=" + starttime + ", dealdatetime=" + dealdatetime
				+ ", ts=" + ts + "]";
	}

}