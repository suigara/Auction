package org.ww.jd.auction;

import static org.ww.jd.utils.HttpClientUtils.doGetJson;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.ww.jd.JDURLConsts;
import org.ww.jd.log.Logger;

public class ServerTimeUtil {
	private static Map<String, Long> goodsTimesDiff = new HashMap<>();
	private static Map<String, Long> goodsTimesTs = new HashMap<>();

	public static Long getServerTime(String goodsId) {
		String curl = JDURLConsts.AUCTION_URL + "/" + goodsId;
		long time = Calendar.getInstance().getTime().getTime();
		if (goodsTimesDiff.containsKey(goodsId)) {
//			long ts = goodsTimesTs.get(goodsId);
			// 10��������
			// if ((time - ts) < 1000 * 60 * 30) {
			return time + goodsTimesDiff.get(goodsId);
			// }
			// return time + goodsTimesDiff.get(goodsId);
		}
		try {

			JSONObject json = doGetJson(UnSessionUtils.httpclient,
					"http://auction.jd.com/json/paimai/now?t=" + time, curl);
			long servertime = Long.valueOf(json.getLong("now"));
			goodsTimesDiff.put(goodsId, servertime - time);
			goodsTimesTs.put(goodsId, time);
			return servertime;
		} catch (Exception e) {
			Logger.info(goodsId + " getServerTime error:" + e.getMessage());
			Logger.error(e);
			return time;
		}
	}
}
