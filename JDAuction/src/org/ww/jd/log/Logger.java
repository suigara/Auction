package org.ww.jd.log;

public class Logger {

	public static void info(Object msg) {
		logImpl().info(toString(msg));
	}
	
	public static void info(String msg) {
		logImpl().info(msg);
	}

	public static void debug(Object msg) {
		logImpl().debug(toString(msg));
	}
	public static void debug(String msg) {
		logImpl().debug(msg);
	}

	public static void warn(Object msg) {
		logImpl().warn(toString(msg));
	}

	public static void error(Throwable e) {
		logImpl().error(e.getMessage(), e);
	}

	public static void error(String msg) {
		logImpl().error(toString(msg));
	}

	public static void trace(Throwable e) {
		logImpl().trace(e.getMessage(), e);
	}

	public static void trace(String msg, Throwable e) {
		logImpl().trace(msg, e);
	}

	private static String toString(Object msg) {
		if (msg == null) {
			return null;
		}
		return msg.toString();
	}

	private static org.slf4j.Logger logImpl() {
		return LogPlugin.getLogger();
	}
}
